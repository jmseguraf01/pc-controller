import time
from datetime import datetime

import pyautogui

from Funciones import Escribir_teclado, Hacer_click, Mover_cursor, Youtube, Scrollbar, Desconectar
import psycopg2




def main():
    usuario = input("Usuario: ")
    password = input("Password: ")
    try:
        # Login
        conexion = psycopg2.connect(host="192.168.1.201", database="pc_controller", user=usuario, password=password)
        while True:
            cursor = conexion.cursor()
            cursor.execute("SELECT * FROM controller where id = 1")
            # Cojo cada uno de los datos de la BBDD
            x = 0
            for todo in cursor.fetchall():
                cursorx = todo[0]
                cursory = todo[1]
                buscar = todo[2]
                keyboard = todo[3]
                click = todo[4]
                logout = todo[5]
                scrollbar_bajar = todo[6]
                scrollbar_subir = todo[7]
                yt_anterior = todo[8]
                yt_siguiente = todo[9]

            # Ejecuto
            Mover_cursor.main(cursorx, cursory)
            click = Hacer_click.main(click)
            Escribir_teclado.escribir(keyboard)
            keyboard = Escribir_teclado.buscar(buscar)
            buscar = False
            yt_siguiente = Youtube.yt_siguiente(yt_siguiente)
            yt_anterior = Youtube.yt_anterior(yt_anterior)
            scrollbar_subir = Scrollbar.scrollbar_subir(scrollbar_subir)
            scrollbar_bajar = Scrollbar.scrollbar_bajar(scrollbar_bajar)
            desconectar = Desconectar.main(logout)

            # Actualizo la base de datos
            sql = "update controller set buscar = %s, keyboard = %s, click = %s, yt_siguiente = %s, yt_anterior = %s, scrollbar_subir = %s, scrollbar_bajar = %s where id = 1"
            val = buscar, keyboard, click, yt_siguiente, yt_anterior, scrollbar_subir, scrollbar_bajar
            cursor.execute(sql, val)
            # Guardo los cambios
            conexion.commit()

            # Si hay que desconectar
            if (desconectar):
                conexion.close()
                exit()

            # Necesario para que el programa coja bien los datos
            # time.sleep(0.4)
            

    # Error de autentificacion
    except psycopg2.OperationalError:
        print("User o password error")

    # Cuando peta
    except pyautogui.FailSafeException:
        conexion.close()
        exit()

# Pinta datos en la pantalla
def pintar():
    print("")
    print("Programa iniciado a las: %s." % (datetime.now().strftime("%H:%M:%S")))

# Inicio
if __name__ == '__main__':
    pintar()
    main()
