import keyboard

# Funcion que comprueba si hay que adelantar el video de youtube
def yt_siguiente(yt_siguiente):

    # Si se tiene que adelantar 5s el video de yt
    if yt_siguiente == True:
        keyboard.press_and_release("right")

    return False


# Funcion que comprueba si hay que adelantar el video de youtube
def yt_anterior(yt_anterior):

    # Si se tiene que adelantar 5s el video de yt
    if yt_anterior == True:
        keyboard.press_and_release("left")

    return False