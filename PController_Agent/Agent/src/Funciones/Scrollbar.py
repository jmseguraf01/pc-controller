
import keyboard

veces_mover_scrollbar = 6

# Comprueba si hay que subir la barra de desplazamiento
def scrollbar_subir(scrollbar):
    if scrollbar == True:
        # Subo la barra de desplazamiento
        for x in range(0,veces_mover_scrollbar):
            keyboard.press_and_release("up")
    return False;

# Comprueba si hay que bajar la barra de desplazamiento
def scrollbar_bajar(scrollbar):
    if scrollbar == True:
        # Subo la barra de desplazamiento
        for x in range(0, veces_mover_scrollbar):
            keyboard.press_and_release("down")

    return False