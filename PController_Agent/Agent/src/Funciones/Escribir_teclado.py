import keyboard

class Keyboard:
    # Contenido del fichero keyboard antiguo
    contenido_antiguo = ""

# Funcion que comprueba si hay que escribir algo y lo escribe
def escribir(contenido_keyboard):
    # Si hay texto en el fichero y no es el mismo que antes, lo escribo en el PC
    if (contenido_keyboard != "" and Keyboard.contenido_antiguo != contenido_keyboard):
        # Selecciono el texto que ya haya escrito
        keyboard.press_and_release("ctrl+a")
        keyboard.write(contenido_keyboard, delay=0)

    Keyboard.contenido_antiguo = contenido_keyboard

# Funcion que comprueba si hay que buscar el texto escrito
def buscar(buscar):
    # Si pone true, busco en el PC el texto escrito
    if buscar == True:
        keyboard.press_and_release("enter")

    return ""
