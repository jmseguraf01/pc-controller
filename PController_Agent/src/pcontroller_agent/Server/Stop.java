package pcontroller_agent.Server;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pcontroller_agent.Main;
import pcontroller_agent.Status;

public class Stop {
    private final String apagarServidor = "\"C:\\Program Files\\Oracle\\VirtualBox\\VBoxManage.exe\" controlvm ba4ff559-2d4d-47a8-9f23-68c3c3dd7a5b acpipowerbutton";
    private static String fileLogout = "Agent/files/logout.dat";
    
    // Paro el servidor y el programa
    public void stop() {
        try {
            // Apago el servidor
            new ProcessBuilder("cmd.exe", "/c", apagarServidor).start();
            // Actualizo status
            Main.status = Status.apagandose;
            // Cambio la salida del audio
            new ProcessBuilder("cmd.exe", "/c", "Agent\\Libs\\nircmd.exe setdefaultsounddevice \"M2280D\" ").start();
            // Mientras este activo el agente
            while (Main.p.isAlive()) {                
                // Escribo true en el fichero de salir
                FileWriter fileWriter = new FileWriter(fileLogout);
                fileWriter.write("true");
                fileWriter.close();
                Thread.sleep(500);
            }
            // Compruebo si el servidor se ha apagado
            new java.util.Timer().schedule( 
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        new ComprobarServer().comprobarServer();
                    }
            }, 5000);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
