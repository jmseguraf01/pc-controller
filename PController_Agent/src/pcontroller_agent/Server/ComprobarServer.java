package pcontroller_agent.Server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pcontroller_agent.Main;
import pcontroller_agent.Status;



public class ComprobarServer {
    
    Boolean encendido;
    
    // Funcion que comprueba si el servidor esta encendido o no
    public void comprobarServer() {
        String ip = "192.168.1.55";
        encendido = false;
        try {
            InetAddress inet = InetAddress.getByName(ip);
            encendido = inet.isReachable(1000);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ComprobarServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ComprobarServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Cambio el status
        if (encendido) {
            Main.status = Status.servidorEncendido;
        } else {
            Main.status = Status.servidorApagado;
        }
        
    }
}
