package pcontroller_agent.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import pcontroller_agent.Main;
import pcontroller_agent.Status;

public class Start {
    String startAgent = "start_agent.bat";
    private final String encenderServidor = " \"C:\\Program Files\\Oracle\\VirtualBox\\VBoxManage.exe\" startvm ba4ff559-2d4d-47a8-9f23-68c3c3dd7a5b --type headless";

    // Inicio el servidor y el programa
    public void start() {
        try {        
            // Enciendo el servidor
            new ProcessBuilder("cmd.exe", "/c", encenderServidor).start();
            // Actualizo el estado
            Main.status = Status.encendiendose;
            // Cambio la salida del audio
            ProcessBuilder audio = new ProcessBuilder("cmd.exe", "/c",  "Agent\\Libs\\nircmd.exe setdefaultsounddevice \"32LG3000\" ");
            audio.redirectErrorStream(true);
            final Process process = audio.start();
            
            
            InputStream stderr = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(stderr);
            BufferedReader br = new BufferedReader(isr);
            String line = null;


            while ((line = br.readLine()) != null) {
                System.out.println(line);

            }
            
            // Compruebo si se ha encendido el servidor
            new Timer().schedule( 
                new TimerTask() {
                    @Override
                    public void run() {
                        new ComprobarServer().comprobarServer();
                    }
            }, 60000);
            // Ejecuto el programa agente
            new Timer().schedule( 
                new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            Main.p = Runtime.getRuntime().exec(startAgent);
                            Main.status = Status.ejecutandose;
                        } catch (IOException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
            }, 62000 );

            

        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
