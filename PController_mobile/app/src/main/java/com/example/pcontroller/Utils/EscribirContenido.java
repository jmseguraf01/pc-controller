package com.example.pcontroller.Utils;

import android.content.Context;

import java.io.FileOutputStream;
import java.io.IOException;

// Escribo el contenido en un fichero local
public class EscribirContenido {
    public EscribirContenido(String texto, Context context, String fileLocal) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = context.openFileOutput(fileLocal, context.MODE_PRIVATE);
            fileOutputStream.write(texto.getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
