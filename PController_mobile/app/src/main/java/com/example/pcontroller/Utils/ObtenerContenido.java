package com.example.pcontroller.Utils;

import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ObtenerContenido {
    // Funcion que obtiene el contenido del fichero remoto
    public String main(FTPClient ftpClient, String fileRemote) {
        String contenido = null;
        int data = 0;
        try {
            InputStream inStream = ftpClient.retrieveFileStream(fileRemote);
            InputStreamReader isr = new InputStreamReader(inStream, "UTF8");
            data = isr.read();
            contenido = "";
            while(data != -1){
                char theChar = (char) data;
                contenido = contenido + theChar;
                data = isr.read();
            }
            isr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return contenido;
    }
}