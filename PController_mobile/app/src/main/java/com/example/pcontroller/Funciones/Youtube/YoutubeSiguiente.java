package com.example.pcontroller.Funciones.Youtube;

import android.content.Context;
import android.os.AsyncTask;

import com.example.pcontroller.Utils.EscribirContenido;
import com.example.pcontroller.Utils.SubirFichero;

public class YoutubeSiguiente {
    private String fileLocal = "yt_siguiente.dat";
    private String fileRemote = "yt_siguiente.dat";
    private String texto = "true\n";
    private Context context;

    public void main(Context context) {
        this.context = context;
        new FTPTask().execute();
    }

    // Clase para conectarme con el servidor
    private class FTPTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            // Escribo el contenido el en fichero local
            new EscribirContenido(texto, context, fileLocal);
            // Subo el fichero al servidor
            new SubirFichero(context.getFilesDir() + "/" + fileLocal, fileRemote);
            return null;
        }
    }

}
