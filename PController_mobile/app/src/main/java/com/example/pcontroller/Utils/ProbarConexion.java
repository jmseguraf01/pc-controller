package com.example.pcontroller.Utils;

import android.os.AsyncTask;

import com.example.pcontroller.Ftp.ConnectFTP;

import org.apache.commons.net.ftp.FTPClient;


public class ProbarConexion {
    // Boolean que indica  si el servidor esta encendido o no
    private boolean servidorEncendido;

    // Compruebo si la conexion con el servidor no es nula
    public boolean probarConexion() {
        new FTPTask().execute();
//      Pongo una espera para que le de tiempo al task a cambiar el boolean
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//      Le hago un ping para comprobar si esta encendido
        return servidorEncendido;
    }

    // Clase para conectarme con el servidor
    private class FTPTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            ConnectFTP connectFTP = new ConnectFTP();
            FTPClient ftpClient = connectFTP.crearConexion();
            // El servidor esta encendido
            if (ftpClient != null) {
                servidorEncendido = true;
                // Desconecto
                connectFTP.cerrarConexion(ftpClient);
            }
            return null;
        }
    }

}
