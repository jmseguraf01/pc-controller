package com.example.pcontroller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class PantallaInicio extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_inicio);
        // Quito la barra de menu de arriba
        getSupportActionBar().hide();
        // Abro la otra pantalla despues de 1 minuto
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent pantallaJuego = new Intent(PantallaInicio.this, MainActivity.class);
                startActivity(pantallaJuego);
                finish();
            }
        }, 1000);

    }
}