package com.example.pcontroller.Funciones.Scrollbar;

import android.content.Context;
import android.os.AsyncTask;

import com.example.pcontroller.Utils.EscribirContenido;
import com.example.pcontroller.Utils.SubirFichero;

public class ScrollbarSubir {
    private String fileLocal = "scrollbar_subir.dat";
    private String fileRemote = "scrollbar_subir.dat";
    private String texto = "true\n";
    private Context context;

    public void main(Context context) {
        this.context = context;
        new FTPTask().execute();
    }


    // Clase para conectarme con el servidor
    private class FTPTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            // Escribo el texto que ha puesto el usuario en el fichero
            new EscribirContenido(texto, context, fileLocal);
            new SubirFichero(context.getFilesDir() + "/" + fileLocal, fileRemote);
            return null;
        }
    }

}
