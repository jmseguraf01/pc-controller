package com.example.pcontroller.Funciones;

import android.content.Context;
import android.os.AsyncTask;

import com.example.pcontroller.Utils.EscribirContenido;
import com.example.pcontroller.Utils.SubirFichero;

public class Desconectar {
    private String fileLocal = "logout.dat";
    private String fileRemote = "logout.dat";
    private String texto = "true\n";
    private Context context;

    public void main(Context context) {
        this.context = context;
        new FTPTask().execute();
    }


    // Clase para conectarme con el servidor
    private class FTPTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            // Escribo el texto que ha puesto el usuario en el fichero
            new EscribirContenido(texto, context, fileLocal);
            new SubirFichero(context.getFilesDir() + "/" + fileLocal, fileRemote);
            return null;
        }
    }
}
