package com.example.pcontroller.Ftp;

import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;

public class ConnectFTP {


    // Crea una conexion
    public FTPClient crearConexion() {
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(DatosFTP.serverIP);
            ftpClient.login(DatosFTP.usuario, DatosFTP.password);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ftpClient;
    }

    // Cierra la conexion
    public FTPClient cerrarConexion(FTPClient ftpClient) {
        try {
            ftpClient.logout();
            ftpClient.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ftpClient;
    }

}
