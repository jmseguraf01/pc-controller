package com.example.pcontroller.Funciones;

import android.content.Context;
import android.os.AsyncTask;

import com.example.pcontroller.Utils.EscribirContenido;
import com.example.pcontroller.Utils.SubirFichero;

public class BuscarTexto {
    private String fileLocal = null;
    private String fileRemote = null;
    private String texto;
    private Context context;

    public void escribirTexto(String texto, Context context) {
        this.fileLocal = "keyboard.dat";
        this.fileRemote = "keyboard.dat";
        this.texto = texto;
        this.context = context;
        new FTPTask().execute();
    }

    public void buscar(Context context) {
        this.texto = "true";
        this.context = context;
        this.fileLocal = "buscar.dat";
        this.fileRemote = "buscar.dat";
        new FTPTask().execute();
    }


    // Clase para conectarme con el servidor
    private class FTPTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            // Escribo el texto que ha puesto el usuario en el fichero
            new EscribirContenido(texto, context, fileLocal);
            new SubirFichero(context.getFilesDir() + "/" + fileLocal, fileRemote);
            return null;
        }
    }

}
