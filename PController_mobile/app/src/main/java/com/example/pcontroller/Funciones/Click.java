package com.example.pcontroller.Funciones;

import android.content.Context;
import android.os.AsyncTask;

import com.example.pcontroller.Ftp.ConnectFTP;
import com.example.pcontroller.Utils.EscribirContenido;
import com.example.pcontroller.Utils.ObtenerContenido;
import com.example.pcontroller.Utils.SubirFichero;

public class Click {
    private final String fileRemote = "click.dat";
    private final String fileLocal = "click.dat";
    private String texto = "true\n";

    // Cargo las clases
    ObtenerContenido obtenerContenido = new ObtenerContenido();
    ConnectFTP connectFTP = new ConnectFTP();

    Context context;


    public void main(Context context) {
        this.context = context;
        new FTPTask().execute();
    }

    // Clase para conectarme con el servidor
    private class FTPTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            // Escribo el contenido el en fichero local
            new EscribirContenido(texto, context, fileLocal);
            // Subo el fichero al servidor
            new SubirFichero(context.getFilesDir() + "/" + fileLocal, fileRemote);
            return null;
        }
    }

}
