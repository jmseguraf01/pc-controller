package com.example.pcontroller;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;

import com.example.pcontroller.Funciones.BuscarTexto;
import com.example.pcontroller.Funciones.Click;
import com.example.pcontroller.Funciones.Desconectar;
import com.example.pcontroller.Funciones.MoveCursor;
import com.example.pcontroller.Funciones.Scrollbar.ScrollbarBajar;
import com.example.pcontroller.Funciones.Scrollbar.ScrollbarSubir;
import com.example.pcontroller.Utils.ProbarConexion;
import com.example.pcontroller.Funciones.Youtube.YoutubeAnterior;
import com.example.pcontroller.Funciones.Youtube.YoutubeSiguiente;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements NumberPicker.OnValueChangeListener {
    private static int valor = 30;

    Button botonArriba, botonAbajo, botonDerecha, botonIzquierda;
    NumberPicker numberPickerValor;

    // Añado los items
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botonArriba = findViewById(R.id.buttonArriba);
        botonAbajo = findViewById(R.id.buttonAbajo);
        botonDerecha = findViewById(R.id.buttonDerecha);
        botonIzquierda = findViewById(R.id.buttonIzquierda);
        numberPickerValor = findViewById(R.id.numberPickerValor);
        // Compruebo la conexion con el servidor
        comprobarConexion();
        // Configuro en number picket
        configurarNumberPicket();
        // Configuro long click
        configureLongClick();
    }

    // Cargo el menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_options, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // Compruebo la conexion con el servidor
    private void comprobarConexion() {
        ProbarConexion probarConexion = new ProbarConexion();
        boolean conexion = probarConexion.probarConexion();
        // Si no hay conexion con el servidor
        if (!conexion) {
            new AlertDialog.Builder(this)
                    .setTitle("Error de conexión")
                    .setMessage("No hay conexion con el servidor, asegurate de que este encendido.")
                    .setPositiveButton("Salir", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    // Funcion para configurar el numberpicket por defecto
    private void configurarNumberPicket() {
        // Valores minimos y maximos para configurar el number picket
        int minimo = 10;
        int maximo = 100;
        // Array de numeros del 0 al 100
        String[] numbers = new String[maximo / 10];
        // Añado los numeros al array de 10 en 10
        for (int i = minimo; i <= maximo; i = i + 10) {
            numbers[(i / 10) - 1] = String.valueOf(i);
        }
        numberPickerValor.setMinValue(0);
        numberPickerValor.setMaxValue(numbers.length - 1);
        numberPickerValor.setValue(Integer.parseInt(numbers[1])/10);
        numberPickerValor.setDisplayedValues(numbers);
        numberPickerValor.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPickerValor.setOnValueChangedListener(this);
    }


    // Cuando cambia el number picket
    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        // Cuando cambia el numberPicket
        valor = numberPickerValor.getValue() * 10 + 10;
    }


    // Click en cualquier boton para mover el cursor
    public void clickMoveCursor(View view) {
        String desplazamiento = null;
        int valorMovimiento = 0;

        // Obtengo el ID del boton clickado
        int id = view.getId();

        switch (id) {
            // Subir
            case R.id.buttonArriba:
                desplazamiento = "y";
                valorMovimiento = -valor;
                break;

            // Bajar
            case R.id.buttonAbajo:
                desplazamiento = "y";
                valorMovimiento = valor;
                break;

            // Derecha
            case R.id.buttonDerecha:
                desplazamiento = "x";
                valorMovimiento = valor;
               break;

           // Izquierda
            case R.id.buttonIzquierda:
                desplazamiento = "x";
                valorMovimiento = -valor;
                break;
        }
        MoveCursor moveCursor = new MoveCursor();
        moveCursor.main(this, desplazamiento, valorMovimiento);
    }

    // Click en el boton "CLICK"
    public void click(View view) {
        Click click = new Click();
        click.main(this);
    }

    // Cuando le das click a los items de anterior o siguiente
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Click en el item siguiente
            case R.id.item_siguiente:
                YoutubeSiguiente youtubeSiguiente = new YoutubeSiguiente();
                youtubeSiguiente.main(this);
                break;

            // Click en el item anterior
            case R.id.item_anterior:
                YoutubeAnterior youtubeAnterior = new YoutubeAnterior();
                youtubeAnterior.main(this);
                break;

            // Click en el item buscar
            case R.id.item_biscar:
                // Muestro el layout buscar como un dialog
                AlertDialog.Builder alertDialogBuscar = new AlertDialog.Builder(MainActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View view = inflater.inflate(R.layout.dialog_buscar, null);
                alertDialogBuscar.setView(view);
                final AlertDialog dialog = alertDialogBuscar.create();
                dialog.show();
                // Creo los botones, editText los enlazo
                final Button botonBuscar = view.findViewById(R.id.buttonBuscar);
                Button botonCancelar = view.findViewById(R.id.buttonCancelar);
                final EditText editTextBuscar = view.findViewById(R.id.editTextBuscar);
                // Cuando se escribe algo en el edittext, se actualiza en el fichero del servidor
                editTextBuscar.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        new BuscarTexto().escribirTexto(editTextBuscar.getText().toString(), MainActivity.this);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
                // Click al boton buscar
                botonBuscar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new BuscarTexto().buscar(MainActivity.this);
                        dialog.dismiss();
                    }
                });
                // Click al boton cancelar
                botonCancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Borro el texto que haya sido escrito en el fichero
                        new BuscarTexto().escribirTexto("", MainActivity.this);
                        dialog.dismiss();
                    }
                });
                break;

            // Click en el item logout
            case R.id.item_logout:
                new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Logout")
                    .setMessage("¿Estas seguro de que te deseas desconectar de la aplicacion?")
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setPositiveButton("Desconectar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Llamo a la funcion y cierro el programa con un retardo
                            new Desconectar().main(MainActivity.this);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    System.exit(0);
                                }
                            }, 500);
                        }
                    })
                    .show();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    // Click largo
    private void configureLongClick() {
        View.OnLongClickListener listenerLongClick = new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                switch (v.getId()) {
                    // Boton subir
                    case R.id.buttonArriba:
                        new ScrollbarSubir().main(MainActivity.this);
                        break;
                    // Boton bajar
                    case R.id.buttonAbajo:
                        new ScrollbarBajar().main(MainActivity.this);
                        break;
                }
                return true;
            }
        };
        botonArriba.setOnLongClickListener(listenerLongClick);
        botonAbajo.setOnLongClickListener(listenerLongClick);
    }

}

