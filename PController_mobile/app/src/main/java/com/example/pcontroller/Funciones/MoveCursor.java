package com.example.pcontroller.Funciones;

import android.content.Context;
import android.os.AsyncTask;

import com.example.pcontroller.Ftp.ConnectFTP;
import com.example.pcontroller.Ftp.DatosFTP;
import com.example.pcontroller.Utils.EscribirContenido;
import com.example.pcontroller.Utils.ObtenerContenido;
import com.example.pcontroller.Utils.SubirFichero;

import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class MoveCursor {

    private static final String fileRemote = "cursor.dat";
    private static final String fileLocal = "cursor.dat";

    private Context context = null;
    private String desplazamiento;
    private int valorMovimiento;

    // Creo las clases
    ConnectFTP connectFTP = new ConnectFTP();
    ObtenerContenido obtenerContenido = new ObtenerContenido();


    public void main(Context context, String desplazamiento, int valorMovimiento) {
        // Actualizo las variables
        this.context = context;
        this.desplazamiento = desplazamiento;
        this.valorMovimiento = valorMovimiento;
        // Conecto con el servidor
        new FTPTask().execute();
    }

    // Clase para conectarme con el servidor
    private class FTPTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                FTPClient ftpClient = connectFTP.crearConexion();
                ftpClient.enterLocalPassiveMode();
                ftpClient.changeWorkingDirectory(DatosFTP.directoryRemote);
                // Obtengo el contenido del fichero remoto
                String contenidoRemoto = obtenerContenido.main(ftpClient, fileRemote);
                // Desconecto
                connectFTP.cerrarConexion(ftpClient);
                // Transformo el contenido a lo que yo quiera
                String contenidoLocal = transformarContenido(contenidoRemoto);
                // Escribo el contenido mio en un fichero local
                new EscribirContenido(contenidoLocal, context, fileLocal);
                // Subo el fichero al servidor
                new SubirFichero(context.getFilesDir() + "/" + fileLocal, fileRemote);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    // Transoformo el contenido del servidor a lo que quiero subir
    private String transformarContenido(String contenido) {
        // Leo el texto por lineas
        String[] linesString = contenido.split(System.getProperty("line.separator"));
        // Lo paso de string a entero
        int[] lines = {Integer.parseInt(linesString[0]), Integer.parseInt(linesString[1])};

        // Muevo el cursor en la posicion X
        if (desplazamiento.equals("x")) {
            lines[0] = Integer.parseInt(linesString[0]) + valorMovimiento;
        }
        // Muevo el cursor en la posicion Y
        else if (desplazamiento.equals("y")) {
            lines[1] = Integer.parseInt(linesString[1]) + valorMovimiento;
        }

        // Actualizo el texto a escribir en el fichero
        contenido = lines[0] + "\n" + lines[1] + "\n";
        return contenido;
    }


    private void loadfile() {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = context.openFileInput(fileLocal);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();
            String text;

            while ((text = bufferedReader.readLine()) != null) {
                stringBuilder.append(text).append("\n");
            }

            System.out.println(stringBuilder);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
