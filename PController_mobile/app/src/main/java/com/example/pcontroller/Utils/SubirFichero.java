package com.example.pcontroller.Utils;

import com.example.pcontroller.Ftp.ConnectFTP;
import com.example.pcontroller.Ftp.DatosFTP;

import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class SubirFichero {

    // Funcion que sube el fichero al servidor
    public SubirFichero(String fileLocal, String fileRemote) {
        // Creo las clases
        ConnectFTP connectFTP = new ConnectFTP();

        try {
            FTPClient ftpClient = connectFTP.crearConexion();
            ftpClient.enterLocalPassiveMode();
            ftpClient.changeWorkingDirectory(DatosFTP.directoryRemote);
            // Subo el fichero
            FileInputStream inputStream = new FileInputStream(new File(fileLocal));
            ftpClient.storeFile(fileRemote, inputStream);
            // Cierro conexion
            connectFTP.cerrarConexion(ftpClient);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
